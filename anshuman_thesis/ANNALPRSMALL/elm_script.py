import cPickle
import numpy as np
from elm import ELMClassifier
from sklearn import linear_model
import glob, os
import cv2
import Image
import re
import matplotlib.pyplot as plt

from skimage.feature import hog
from skimage import data, color, exposure
from sklearn.externals import joblib


import os, re, sys, getopt, pickle

STANDARD_SIZE=(32,32)

def get_hog_data(imagefullpathname):
    
    image = Image.open(imagefullpathname).resize(STANDARD_SIZE).convert('L')
    image = np.asarray(image)

##    fd, hog_image = hog(image, orientations=8, pixels_per_cell=(16, 16),
##        cells_per_block=(1, 1), visualise=True)
    fd, hog_image = hog(image, orientations=8, pixels_per_cell=(16, 16),
        cells_per_block=(1, 1), visualise=True)

##    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4))
##
##    ax1.axis('off')
##    ax1.imshow(image, cmap=plt.cm.gray)
##    ax1.set_title('Input image')

    # Rescale histogram for better display
    hog_image_rescaled = exposure.rescale_intensity(hog_image, in_range=(0, 0.02))

##    ax2.axis('off')
##    ax2.imshow(hog_image_rescaled, cmap=plt.cm.gray)
##    ax2.imshow(hog_image, cmap=plt.cm.gray)
##    ax2.set_title('Histogram of Oriented Gradients')
##    plt.show()
    print type(fd);
    return fd;


def get_training_data():

    

    labels, training_data = [], []
    #dir = os.curdir  + '\\test_data;
    dir = sys.argv[1]
    print 'got dir', dir
    for root, dirs, files in os.walk(dir):
	
	for name in files:
                print 'i am matching'
                #do simple character matches
                match = re.search(r"^([0-9A-Z]?).bmp$", name)
		if match:
                    print name
                    label = match.group(1)
                    print type(label)
                    imagefullpathname = dir+'\\'+name;
                    image = Image.open(imagefullpathname).resize(STANDARD_SIZE).convert('L')
                    image = np.asarray(image)
                else:
                    match = re.search(r"^fill([0-9A-Z]?).bmp$", name)
                    if match:
                        print name
                        label = match.group(1)
                        print type(label)
                        imagefullpathname = dir+'\\'+name;
                        image = Image.open(imagefullpathname).resize(STANDARD_SIZE).convert('L')
                        image = np.asarray(image)
                    else:
                        match = re.search(r"^fill([0-9A-Z]?)\_2.bmp$", name)
                        if match:
                            print name
                            label = match.group(1)
                            print type(label)
                            imagefullpathname = dir+'\\'+name;
                            image = Image.open(imagefullpathname).resize(STANDARD_SIZE).convert('L')
                            image = np.asarray(image)

##                    fd, hog_image = hog(image, orientations=8, pixels_per_cell=(16, 16),
##                        cells_per_block=(1, 1), visualise=True)
                if match:
                    fd, hog_image = hog(image, orientations=8, pixels_per_cell=(16, 16),
                        cells_per_block=(1, 1), visualise=True)

##                    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4))
##
##                    ax1.axis('off')
##                    ax1.imshow(image, cmap=plt.cm.gray)
##                    ax1.set_title('Input image')

                    # Rescale histogram for better display
##                    hog_image_rescaled = exposure.rescale_intensity(hog_image, in_range=(0, 0.02))
##
##                    ax2.axis('off')
##                    ax2.imshow(hog_image_rescaled, cmap=plt.cm.gray)
##                    ax2.set_title('Histogram of Oriented Gradients')
##                    plt.show()
                if match:
                    label=ord(label);
                    labels.append(label)

                    
                    training_data.append(fd)
                    print(fd.size)

    return labels, training_data

def train_classifier():
    labels, training_data = get_training_data()
    # Build ELM
    #cls = ELMClassifier(n_hidden=7000,
    cls = ELMClassifier(n_hidden=400,
                    alpha=0.93,
                   activation_func='gaussian',
                    regressor=linear_model.Ridge(),
                    random_state=21398023)
    cls.fit(training_data, labels)

    
    
    #test_fd = get_hog_data('1.bmp');
    #label_out = cls.predict(test_fd);
    #print "label yahoo ",   chr(label_out)
   

    return cls

def get_testsets(cls):
    

    outputFolder = 'C:\\Users\\user\\Documents\\sscet\\me\\thesis\\LPR\outputelm\\'
    

    lpcharnumfile = outputFolder + 'licenseplatetotalcharnum.txt' 

    
    lpcnf = open(lpcharnumfile)
    line = lpcnf.readline()
    lpcn = int(line)
    print(lpcn)
    
    lpcnf.close();
    data=[]
    labels=[]

    for i in range(1, lpcn+1):
        lpcharfilestr = outputFolder + 'licenseplatechar' + str(i) + '.bmp'
        print "trying to read ", lpcharfilestr
        fd = get_hog_data(lpcharfilestr)
        data.append(fd)

    labels = cls.predict(data)

    labels_out=[]
    for label in labels:
        labels_out.append(chr(label))
    print(labels_out)
        

if __name__ == '__main__':

    
    classifier = train_classifier()
    joblib.dump(classifier, 'my_elm.pkl', compress=9)
    #get_testsets(classifier)
    
