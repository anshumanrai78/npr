# import the necessary packages
from sklearn.cross_validation import train_test_split
from sklearn.metrics import classification_report
from sklearn import datasets
from nolearn.dbn import DBN
import numpy as np
import cv2
import scipy.io
import sys
import cv2
import Image
import os
import glob
from sklearn.externals import joblib
import cPickle
import matplotlib.pyplot as plt

from skimage.feature import hog
from skimage import data, color, exposure
from skimage import morphology

STANDARD_SIZE=(32,32)


from PIL import  ImageChops

def trim(im):
    bg = Image.new(im.mode, im.size, im.getpixel((0,0)))
    diff = ImageChops.difference(im, bg)
    diff = ImageChops.add(diff, diff, 2.0, -100)
    bbox = diff.getbbox()
    if bbox:
        return im.crop(bbox)


def get_features_data(imagefullpathname):
    
    image = Image.open(imagefullpathname)
    
    image = image.convert('L')
    image = trim(image)
    image = image.resize(STANDARD_SIZE)
    image = np.asarray(image)
    image = np.invert(image)    
    
    
   
    
    #image = np.invert(image)
    #cv2.imshow('image', image)
    #cv2.waitKey(0)
    #image = image/255.0
    #image = image.astype('int0')

    #image = cv2.threshold(image, 0, 255, cv2.THRESH_OTSU)[1]
    #skel = morphology.skeletonize(image > 0)

##    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4))
##    ax1.imshow(image, cmap=plt.cm.gray, interpolation='nearest')
##    ax1.axis('off')
##    ax2.imshow(skel, cmap=plt.cm.spectral, interpolation='nearest')
##    ax2.contour(image, [0.5], colors='w')
##    ax2.axis('off')
##
##    fig.subplots_adjust(hspace=0.01, wspace=0.01, top=1, bottom=0, left=0, right=1)
##    plt.show()

    

    fd, hog_image = hog(image, orientations=8, pixels_per_cell=(4, 4),
        cells_per_block=(1, 1), visualise=True)
##    fd, hog_image = hog(image, orientations=8, pixels_per_cell=(16, 16),
##        cells_per_block=(1, 1), visualise=True)

##    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4))
##
##    ax1.axis('off')
##    ax1.imshow(image, cmap=plt.cm.gray)
##    ax1.set_title('Input image')

    # Rescale histogram for better display
##    hog_image_rescaled = exposure.rescale_intensity(hog_image, in_range=(0, 0.02))
##
##    ax2.axis('off')
##    ax2.imshow(hog_image_rescaled, cmap=plt.cm.gray)
##    ax2.imshow(hog_image, cmap=plt.cm.gray)
##    ax2.set_title('Histogram of Oriented Gradients')
##    plt.show()
    
    print type(fd);
    return fd, hog_image;

##    image = np.reshape(image, [28*28, ])
##
##    return image, image




def get_training_data():

        trainX = np.array([])
        trainY = np.array([])
        i=0;

        #folder = '.\caffee\\caffe-ocr-master\\training_images\\'
        folder = sys.argv[1]
        max_count = 0
        for (root, dirs, files) in os.walk(folder):
                for f in files:
                        if f.endswith('.png'):
                                max_count+=1

        print 'Found %s files'%(max_count,)

        count = 0
        done = False
        for (root, dirs, files) in os.walk(folder):
                for f in files:
                        if f.endswith('.png'):
                                try:
                                        print 'opening'
                                        

                                        fd, hog_image = get_features_data(os.path.join(root,f))

                                        surround_folder = os.path.split(root)[-1]
                                        print 'got surround folder'
                                        assert len(surround_folder)==1
                                        print 'yo assert'
                                        #labels[count]=ord(surround_folder)-ord('A')
                                        print (surround_folder)
                                        curr_char_ord = ord(surround_folder)
                                        if (surround_folder >= '0' and surround_folder <= '9'):
                                                curr_char_ord = curr_char_ord - ord('0')
                                        else:
                                                curr_char_ord = curr_char_ord - ord('A') + 10
                  
                                        curr_char_ord = int(curr_char_ord)
                                        print curr_char_ord

                                        if (count==0):
                                                trainX = fd
                                                print trainX.shape
                                        else:
                                                print 'concatenating'
                                                print fd.shape
                                                print trainX.shape
                                                trainX = np.vstack((trainX, fd))
                                                print 'after concat'
                                                print trainX.shape
        ##                                if (count==0):
        ##                                        trainY = np.asarray([curr_char_ord])
        ##                                        print trainY.shape
        ##                                else:
        ##                                        print 'concatenating'
        ##                                        curr_char = np.asarray([curr_char_ord])
        ##                                        print curr_char.shape
        ##                                        print trainY.shape
        ##                                        trainY = np.vstack((trainY, curr_char))
        ##                                        print 'after concat'
        ##                                        print trainY.shape

                                        trainY  = np.append(trainY, curr_char_ord);

                                   
                                        count = count + 1
                                        #if (count == 300):
                                        #        done = True;
                                        #        break;
                                                
                                except Exception as e:
                                        print f
                                        print e
                                        print 'hey'
                                        pass
                if done:
                        break
        ##  for i in range(0,count):
        ##    cv2.imshow('image', data[:,:,i])
        ##    cv2.waitKey(0)




        print trainX.shape
        print trainY.shape
        print trainY.size

        #trainX = np.reshape(trainX, [numrealimages/100, 784]);
        ##for i in range(0, numrealimages):
        ##        currimage = trainX[i]
        ##        currimage = np.reshape(currimage, (28, 28))
        ##        currimage = currimage * 255
        ##        cv2.imshow('image',currimage)
        ##        cv2.waitKey(0);
        print trainX.shape[1]

        print 'shapes'
        print trainY.shape
        print trainX.shape

        trainY=trainY.astype(np.int32)

        for j in range(0, count):
                print (trainY[j])
        for i in trainY:
                print type(i)
                print i
                break
        for i in trainX:
                print i.size
                print type(i)
                print i
                for j in i:
                        print type(j)
                        print j
                        break
                break

        # train the Deep Belief Network with 784 input units (the flattened,
        # 28x28 grayscale image), 300 hidden units, 35 output units (one for
        # each possible output classification, which are the alhabets 1-35)
        print 'trainX.size'
        print trainX.size
        print 'trainY.size'
        print trainY.size
        print 'trainX.shape'
        print trainX.shape
        print 'trainY.shape'
        print trainY.shape


##        count = 0
##        s = trainY.size
##        trainXRet=[]
##        trainYRet=[]
##        for count in range(0,2):
##            for x  in range(0,s):
##                print 'x', x
##                print 'count', count
##                trainXcurr = trainX[x]
##                if (count == 0):
##                    trainXRet = trainXcurr
##                else:
##                    trainXRet = np.vstack((trainXRet, trainXcurr))
##                
##                trainYcurr = trainY[x]
##                if (count == 0):
##                    trainYRet = trainYcurr
##                else:
##                    trainYRet = np.append(trainYRet, trainYcurr)
        return (trainX, trainY)










if __name__ == '__main__':


        [trainX, trainY] = get_training_data()

        dbn = DBN(
                #[trainX.shape[1], 600,300, 100, 36],
                [trainX.shape[1], 400 , 36],
                learn_rates = 0.3,
                learn_rate_decays = 0.9,
                epochs = 10,
                verbose = 1)
        dbn.fit(trainX, trainY)

        # compute the predictions for the test data and show a classification
        # report
        print trainX.size
        print trainY.size
        print trainX.shape
        print trainY.shape
        #currimage = np.reshape(currimage, (28,28));
        #v2.imshow('image', currimage)
        #cv2.waitKey(0);
        #currimage = np.reshape(currimage, (784,));
        #img = Image.open('caffee\\caffe-ocr-master\\training_images\\A\\Verdana-8-3.png')
        #img = img.convert('L');
        #img = np.asarray(img)
        #img = np.reshape(img, [784,])
        #pred = dbn.predict(np.atleast_2d(img))
        #print pred
        preds = dbn.predict(trainX)
        print classification_report(trainY, preds)
        joblib.dump(dbn, 'my_dbn.pkl', compress=9)

        sys.exit(0)



        # randomly select a few of the test instances
        for i in np.random.choice(np.arange(0, len(testY)), size = (10,)):
                # classify the digit
                pred = dbn.predict(np.atleast_2d(testX[i]))
         
                # reshape the feature vector to be a 28x28 pixel image, then change
                # the data type to be an unsigned 8-bit integer
                image = (testX[i] * 255).reshape((28, 28)).astype("uint8")
         
                # show the image and prediction
                print "Actual digit is {0}, predicted {1}".format(testY[i], pred[0])
                cv2.imshow("Digit", image)
                cv2.waitKey(0)
