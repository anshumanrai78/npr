
function dummyoneplate(fstr)

bigsampleimg = imread('sample-all-four-line-one-zero.bmp');
bigsampleimg = rgb2gray(bigsampleimg);
base=load(['' 'trained1.mat']); 
fimg = imread(fstr);
imshow(fimg)
disp('original image')
pause()

Demo(fstr);




%Now from the number plate we apply morphological operations



    numberplate = imread('licenceplate.jpg');
    g = numberplate;
    %g=rgb2gray(numberplate); % Converting the RGB (color) image to gray (intensity).

    disp('showing number plate')
    imshow(g)
    
    
    %contrast the license plate
    
    display('original  license plate image');
    imshow(g);
    pause();
    gsize = size(g)
    display(gsize);
    gh = gsize(1);
    display(gh);
    display(round(gh/2));
    pause();
    st1 = strel('disk', round(gh/2));
    tophat_img = imtophat(g, st1);
    bottomhat_img = imbothat(g, st1);
    g = g + tophat_img - bottomhat_img;
    display(g);
    display('contrast license plate image');
    pause();

    g=medfilt2(g,[3 3]); % Median filtering to remove noise.
    se=strel('disk',1); % Structural element (disk of radius 1) for morphological processing.
    gi=imdilate(g,se); % Dilating the gray image with the structural element.
    ge=imerode(g,se); % Eroding the gray image with structural element.
    gdiff=imsubtract(gi,ge); % Morphological Gradient for edges enhancement.
    gdiff=mat2gray(gdiff); % Converting the class to double.
    gdiff=conv2(gdiff,[1 1;1 1]); % Convolution of the double image for brightening the edges.
    gdiff=imadjust(gdiff,[0.5 0.7],[0 1],0.1); % Intensity scaling between the range 0 to 1.
    B=logical(gdiff); % Conversion of the class from double to binary.
    imshow(B)
    disp('before horizontal lines removal')
    pause()
    % Eliminating the possible horizontal lines from the output image of regiongrow
    % that could be edges of license plate.
    er=imerode(B,strel('line',50,0));
    imshow(er)
    disp('after erosion')
    pause()
    out1=imsubtract(B,er);
    imshow(out1)
    disp('after horizontal lines removal')
    pause()
    %elimniate vertical lines
    %er=imerode(out1,strel('line',50,90));
    %disp('checking vertical lines')
    %imshow(er)
    F=imfill(out1,'holes');
    % Thinning the image to ensure character isolation.
    H=bwmorph(F,'thin',1);
    H=imerode(H,strel('line',3,90));
    % Selecting all the regions that are of pixel area more than 100.
    final=bwareaopen(H,10);

    imwrite(final, 'final.jpg')
    disp('after morphological')
    imshow(final);
    


    pause()




    [L,num] = bwlabel(final); %Find components

    disp(num)
    figure();   %Create figure
    imagesc(L);  %Draw the components, each in its own color.
    pause()
    %Get the bounding box Rectangle containing the region
    Sdata=regionprops(L,'BoundingBox');



    for i=1:num
        disp(i)
        heightchar(i) = Sdata(i).BoundingBox(4);
    end

    disp(heightchar)
    pause()

    %find mode of height
    heightmode=mode(heightchar)
    disp('height 1d perim')
    disp(heightmode)

    %find center of image

    center = size(final)/2;

    disp(center(1))
    disp(center(2))

    pause()

    %find which connected component is closest to the center

    stats = regionprops(L, 'Centroid')

    for i=1:num
        s_y(i)=stats(i).Centroid(1)
        s_x(i)=stats(i).Centroid(2)
    end


    mindist = 50000
    mindistindex = 0
    for i=1:num
        currdist = pdist2(center, [s_x(i), s_y(i)], 'euclidean')
        dist(i) = currdist
        if currdist < mindist
            mindist = currdist
            mindistindex = i
        end
    end

    Img = imcrop(final,Sdata(mindistindex).BoundingBox)
    imshow(Img)
    disp('showing center character')
    pause()
    imwrite(Img, 'center.jpg')

    centercharheight = Sdata(mindistindex).BoundingBox(4)

    disp(centercharheight)
    disp(heightchar)
    pause()

    a=[];

    %x = find(height<=largestheight+.1)
    %disp('number of matching characters')
    %disp(length(x))
    noPlate=[]
    for i=1:num
        currheight = heightchar(i)
        deviation = .1 * centercharheight
        if (currheight <= centercharheight + deviation && currheight >= centercharheight - deviation)
            Img = imcrop(final,Sdata(i).BoundingBox);
            %imwrite(Img, strcat('img',num2str(i),'.jpg'))
            letter=readLetter(Img)
            
            disp(letter)
            noPlate = [noPlate letter]
            %%M = (L==x(i))
            %disp('enter')
            %pause()
            %imshow(M)
            
            
            


        end


    end

    
    return

    fid = fopen('noPlate.txt', 'wt'); % This portion of code writes the number plate
    fprintf(fid,'%s\n',noPlate);      % to the text file, if executed a notepad file with the
    fclose(fid);                      % name noPlate.txt will be open with the number plate written.
    winopen('noPlate.txt')
end



