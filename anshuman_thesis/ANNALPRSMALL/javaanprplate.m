function javaanprplate(filestr)

base=load(['' 'trained1.mat']);

import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import javaanpr.imageanalysis.HoughTransformation;
import javaanpr.imageanalysis.Photo;
import javaanpr.configurator.Configurator;
import javaanpr.gui.TimeMeter;
import javaanpr.imageanalysis.Band;
import javaanpr.imageanalysis.CarSnapshot;
import javaanpr.imageanalysis.Char;
import javaanpr.imageanalysis.Plate;
import javaanpr.intelligence.Intelligence;
import javaanpr.intelligence.RecognizedPlate;
import java.lang.Math;
import javaanpr.recognizer.CharacterRecognizer;

%cars = javaanpr.imageanalysis.CarSnapshot('matlab 7.10.0 (2010)\Number_Plate_Extraction\licenseplates\SK01P5456.jpg')
cars = CarSnapshot(filestr)
bs = cars.getBands();
display(bs);
bnum=bs.size();
found = false;
for i=0:bnum-1
    cur_band = bs.elementAt(i);
    ps = cur_band.getPlates();
    pnum = ps.size();
    display(pnum);
    for j=0:pnum-1
        plate = ps.elementAt(j);
        
        
        enableReportGeneration = 0
        
        syntaxAnalysisMode = Intelligence.configurator.getIntProperty('intelligence_syntaxanalysis');
        skewDetectionMode = Intelligence.configurator.getIntProperty('intelligence_skewdetection');
        
        notNormalizedCopy = [];
        renderedHoughTransform = [];
        if (enableReportGeneration || not(skewDetectionMode==0))  % detekcia sa robi but 1) koli report generatoru 2) koli korekcii
            notNormalizedCopy = plate.clone();
            notNormalizedCopy.horizontalEdgeDetector(notNormalizedCopy.getBi());
            hough = notNormalizesdCopy.getHoughTransformation(); 
            renderedHoughTransform = hough.render(HoughTransformation.RENDER_ALL, HoughTransformation.COLOR_BW);
        end
        
        if (not(skewDetectionMode==0)) %korekcia sa robi iba ak je zapnuta
            AffineTransform shearTransform = AffineTransform.getShearInstance(0,-(double)hough.dy/hough.dx);
            BufferedImage core = plate.createBlankBi(plate.getBi());
            core.createGraphics().drawRenderedImage(plate.getBi(), shearTransform);
            plate = Plate(core);
        
        end
        plate.normalize();
                
        plateWHratio = plate.getWidth() / plate.getHeight();
        if ((plateWHratio < Intelligence.configurator.getDoubleProperty('intelligence_minPlateWidthHeightRatio')) ||  (plateWHratio > Intelligence.configurator.getDoubleProperty('intelligence_maxPlateWidthHeightRatio'))) 
            
                continue;
                
        end
                
        chars = plate.getChars();
        
        if (chars.size() < Intelligence.configurator.getIntProperty('intelligence_minimumChars') || chars.size() > Intelligence.configurator.getIntProperty('intelligence_maximumChars')) 
            continue;
        end
                
        if (plate.getCharsWidthDispersion(chars) > Intelligence.configurator.getDoubleProperty('intelligence_maxCharWidthDispersion')) 
            continue;
        end
                
        %ZNACKA PRIJATA, ZACINA NORMALIZACIA A HEURISTIKA PISMEN */

                
                
        
        recognizedPlate = RecognizedPlate();
        numRecognizedChars = 0;
                
        
        chrnum = size(chars);
        for k=0:chrnum-1
            chr = chars.elementAt(k);
            chr.normalize();
        end
                
        averageHeight = plate.getAveragePieceHeight(chars);
        averageContrast = plate.getAveragePieceContrast(chars);
        averageBrightness = plate.getAveragePieceBrightness(chars);
        averageHue = plate.getAveragePieceHue(chars);
        averageSaturation = plate.getAveragePieceSaturation(chars);
                
        for (k=0:chrnum-1) 
            chr = chars.elementAt(k)
            % heuristicka analyza jednotlivych pismen
            ok = true;
            errorFlags = '';
                    
            % pri normalizovanom pisme musime uvazovat pomer
            widthHeightRatio = chr.pieceWidth;
            widthHeightRatio = widthHeightRatio / chr.pieceHeight;
                    
            if (widthHeightRatio < Intelligence.configurator.getDoubleProperty('intelligence_minCharWidthHeightRatio') || widthHeightRatio > Intelligence.configurator.getDoubleProperty('intelligence_maxCharWidthHeightRatio')) 
                errorFlags = strcat(errorFlags,'WHR ');
                ok = false;
                if (not (enableReportGeneration)) 
                    continue;
                end
            end
                    
                    
            if ((chr.positionInPlate.x1 < 2 || chr.positionInPlate.x2 > plate.getWidth()-1) && widthHeightRatio < 0.1) 
                errorFlags = strcat(errorFlags,'POS ');
                ok = false;
                if (not (enableReportGeneration)) 
                    continue;
                end
            end
                    
                    
            %float similarityCost = rc.getSimilarityCost();
                    
            contrastCost = Math.abs(chr.statisticContrast - averageContrast);
            brightnessCost = Math.abs(chr.statisticAverageBrightness - averageBrightness);
            hueCost = Math.abs(chr.statisticAverageHue - averageHue);
            saturationCost = Math.abs(chr.statisticAverageSaturation - averageSaturation);
            heightCost = (chr.pieceHeight - averageHeight) / averageHeight;
                    
            if (brightnessCost > Intelligence.configurator.getDoubleProperty('intelligence_maxBrightnessCostDispersion')) 
                errorFlags = strcat(errorFlags,'BRI ');
                ok = false;
                if (not (enableReportGeneration))
                    continue;
                end
            end
            if (contrastCost > Intelligence.configurator.getDoubleProperty('intelligence_maxContrastCostDispersion')) 
                errorFlags = strcat(errorFlags,'CON ');
                ok = false;
                if (not (enableReportGeneration)) 
                    continue;
                end
            end
            if (hueCost > Intelligence.configurator.getDoubleProperty('intelligence_maxHueCostDispersion')) 
                errorFlags = strcat(errorFlags,'HUE ');
                ok = false;
                if (not (enableReportGeneration)) 
                    continue;
                end
            end
            if (saturationCost > Intelligence.configurator.getDoubleProperty('intelligence_maxSaturationCostDispersion')) 
                errorFlags = strcat(errorFlags,'SAT ');
                ok = false;
                if (not (enableReportGeneration)) 
                    continue;
                end
            end
            if (heightCost < -Intelligence.configurator.getDoubleProperty('intelligence_maxHeightCostDispersion')) 
                errorFlags = strcat(errorFlags,'HEI ');
                ok = false;
                if (not (enableReportGeneration)) 
                    continue;
                end
            end
                    
            similarityCost = 0;
            %rchar = CharacterRecognizer.RecognizedChar;
            if (ok==true)
                    %recognizedPlate.addChar(rchar);
                    numRecognizedChars = numRecognizedChars + 1;
            end
            
        end
                
        % nasledujuci riadok zabezpeci spracovanie dalsieho kandidata na znacku, v pripade ze charrecognizingu je prilis malo rozpoznanych pismen
        %if (recognizedPlate.chars.size() < Intelligence.configurator.getIntProperty('intelligence_minimumChars')) 
        if (numRecognizedChars < Intelligence.configurator.getIntProperty('intelligence_minimumChars'))     
            continue;
        end
        
        
        plate.saveImage('plate.jpg');
        finalplate = plate;
        im = imread('plate.jpg');
        imshow(im);
        pause();
        found = true;
        cur_band.saveImage('band.jpg');
        im = imread('band.jpg');
        imshow(im);
        pause();
        break;
    end
    if(found)
        break;
    end
end

a=[];
if (found)
    finalchars = finalplate.getChars();
    finalchrnum = size(finalchars);
    for k=0:finalchrnum-1
        finalchr = finalchars.elementAt(k);
        %chr.normalize();
        finalchr.saveImage('char.jpg');
        Img = imread('char.jpg');
        bw = im2bw(Img,0.5);
        bw2 = edu_imgcrop(bw);
        charvec = edu_imgresize(bw2);
        figure,imshow(bw);
        pause();

        %anshuman : modified 29/04/2015

        %selected_net = evalin('base','net');

        %result = sim(selected_net, charvec);
        result = sim(base.net,charvec);

        a=[a vec2ind(result)];
    end
end

a
display('size(a)');
display(size(a));
[m,n]= size(a);
lnumber=[];

for i=1:n
if  a(i)==1
    lnumber=[lnumber '0'];
end
    

if a(i)==2
    lnumber=[lnumber '1'];
end

if a(i)==3
    lnumber=[lnumber '2'];
end

if a(i)==4
    lnumber=[lnumber '3'];
end

if a(i)==5
    lnumber=[lnumber '4'];
end

if a(i)==6
    lnumber=[lnumber '5'];
end

if a(i)==7
    lnumber=[lnumber '6'];
end

if a(i)==8
    lnumber=[lnumber '7'];
end

if a(i)==9
    lnumber=[lnumber '8'];
end

if a(i)==10
    lnumber=[lnumber '9'];
end

if a(i)==11
    lnumber=[lnumber '0'];
end


if  a(i)==12
    lnumber=[lnumber 'A'];
end

if a(i)==13
    lnumber=[lnumber 'B'];
end

if a(i)==14
    lnumber=[lnumber 'C'];
end

if a(i)==15
    lnumber=[lnumber 'D'];
end

if a(i)==16
    lnumber=[lnumber 'E'];
end

if a(i)==17
    lnumber=[lnumber 'F'];
end

if a(i)==18
    lnumber=[lnumber 'G'];
end

if a(i)==19
    lnumber=[lnumber 'H'];
end

if a(i)==20
    lnumber=[lnumber 'I'];
end

if a(i)==21
    lnumber=[lnumber 'J'];
end

if a(i)==22
    lnumber=[lnumber 'K'];
end

if  a(i)==23
    lnumber=[lnumber 'L'];
end

if a(i)==24
    lnumber=[lnumber 'M'];
end

if a(i)==25
    lnumber=[lnumber 'N'];
end

if a(i)==26
    lnumber=[lnumber 'O'];
end

if a(i)==27
    lnumber=[lnumber 'P'];
end

if a(i)==28
    lnumber=[lnumber 'Q'];
end

if a(i)==29
    lnumber=[lnumber 'R'];
end

if a(i)==30
    lnumber=[lnumber 'S'];
end

if a(i)==31
    lnumber=[lnumber 'T'];
end

if a(i)==32
    lnumber=[lnumber 'U'];
end

if a(i)==33
    lnumber=[lnumber 'V'];
end

if a(i)==34
    lnumber=[lnumber 'W'];
end

if a(i)==35
    lnumber=[lnumber 'X'];
end

if a(i)==36
    lnumber=[lnumber 'Y'];
end

if a(i)==37
    lnumber=[lnumber 'Z'];
end

end



lnumber
fid=fopen('output.txt','w+');
fprintf(fid,'%s\n',lnumber);
fclose(fid);
winopen('output.txt');

end

