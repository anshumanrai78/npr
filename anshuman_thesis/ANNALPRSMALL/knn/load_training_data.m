function [training class] = load_training_data()

category_num = 36;
cate_train_num = 3;
total_train_num = category_num * cate_train_num;

class = zeros(total_train_num, 1);
training = zeros(total_train_num, 25* 25);

class_count = 1;

alphanumerals = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', ...
    'E', 'F', 'G', 'H', 'G', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', ...
    'U', 'V', 'W', 'X', 'Y', 'Z'];

%for cate=0:9
for idx=1:numel(alphanumerals)
    cate=alphanumerals(idx);
    for n=1:cate_train_num
        %imname = strcat('train/', int2str(cate), '/', int2str(n), '.bmp');
        imname = strcat('train/', cate, '/', int2str(n), '.bmp');
        img = imread(imname);
        
        if (idx >= 1 && idx <= 10 )
            class(class_count) = abs(cate)-'0'+1;
%             display('for character ');
%             display(cate);
%             display(' we get ')
%             display(class(class_count));
%             pause();
        else
            class(class_count) = abs(cate)-abs('A')+11;
%             display('for character ');
%             display(cate);
%             display(' we get ')
%             display(class(class_count));
%             pause();
        end
        training(class_count,:) = im_feature(img);
        
        class_count = class_count + 1;
    end
end
