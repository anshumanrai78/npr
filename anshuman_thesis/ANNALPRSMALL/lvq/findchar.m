function findchar(im, hist, files , net)
    hs = imhist(rgb2gray(imresize(im,[50 50])));
    cls = vec2ind(net(hs));
 
    [~, n] = size(hist);
    for i = 1 : n
        if(cls == vec2ind(net(hist(:, i))))
            figure('name', files(i).name);
            imshow(imresize(imread(files(i).name), [100 100]))
        end
    end
end
