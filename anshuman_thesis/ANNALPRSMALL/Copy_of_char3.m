%% Character Recognition Example (III):Training a Simple NN for
%% classification

%% Read the image
I = imread('sample-all-four-line-one-zero.bmp');

%% Image Preprocessoutg
img = edu_imgpreprocess6(I);
out=[];
%for cnt = 1:50
for cnt = 1:144
    bw2 = edu_imgcrop(img{cnt});
    
    charvec = edu_imgresize(bw2);
    %charvec = feature_extractor(bw2);
    imshow(charvec);
    
    display(size(charvec));
    pause();
    %charvec = bw2;
    out=[out charvec];
   %out(:,cnt) =a
end
%% Create Vectors for the components (objects)
P = out(:,1:108);
%P=out(:,1:50)
T = [eye(36) eye(36) eye(36) eye(36)];
Ptest = out(:,109:144);

%% Creating and training of the Neural Network

%net = edu_createnn(P,T); %using feedforward

i=1:36;
S(i)=1/35;
S(36)=0;

S
C=repmat(i,1,3);
C
T=ind2vec(C);
T
%P=[P P+randn(35,40)*0.1];

display('size of net')
display(size(P));
display(size(minmax(P)));
display(size(T));
pause();
net= newlvq(minmax(P),33,S,0.003); %using lvq
%best working set
%net = newlvq(minmax(P), 35, S, 0.003) with 37 elements
net.trainParam.epochs=500;

display('net params')
display(size(P));
display(size(T));
display(size(S));
pause();
net=train(net,P,T);

%net=newgrnn(P,T,0.5);
%Y=sim(net,P);
save trained1 net
%% Testing the Neural Network
[a,b]=max(sim(net,Ptest));
disp(b);


