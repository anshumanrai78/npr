function lett = edu_imgresize(bw2)
% This function will take the cropped binary image and change it to 50 x 70
% character representation in single vector.

bw_7050=imresize(bw2,[70,50]);
%bw_7050=imresize(bw2,[70,50]);
%lett = reshape(bw_7050, 3500, 1);
%lett=((255-lett)/255);
%lett = lett';

%bw_7050=imresize(bw2,[700,500]);
for cnt=1:7
%for cnt = 1:70
    for cnt2=1:5
    %for cnt2=1:50
       Atemp=sum(bw_7050((cnt*10-9:cnt*10),(cnt2*10-9:cnt2*10)));
        %Atemp=sum(bw_7050((cnt*10-9:cnt*10),(cnt2*10-9:cnt2*10)));
        lett((cnt-1)*5+cnt2)=sum(Atemp);
    end
end

lett=((100-lett)/100);
lett=lett';
%display('sizelett');
%display(size(lett));
%pause();