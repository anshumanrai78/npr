import cPickle
import numpy as np
from elm import ELMClassifier
from sklearn import linear_model
import glob, os
import cv2
import Image
import re
import matplotlib.pyplot as plt

from skimage.feature import hog
from skimage import data, color, exposure


import os, re, sys, getopt, pickle
from sklearn.externals import joblib


STANDARD_SIZE=(32,32)

def get_hog_data(imagefullpathname):

    
    

    image = Image.open(imagefullpathname)
    image=image.convert('L');
    image = image.resize(STANDARD_SIZE)
    image = np.asarray(image)

##    fd, hog_image = hog(image, orientations=8, pixels_per_cell=(16, 16),
##        cells_per_block=(1, 1), visualise=True)
    fd, hog_image = hog(image, orientations=8, pixels_per_cell=(16, 16),
        cells_per_block=(1, 1), visualise=True)

##    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4))
##
##    ax1.axis('off')
##    ax1.imshow(image, cmap=plt.cm.gray)
##    ax1.set_title('Input image')

    # Rescale histogram for better display
    hog_image_rescaled = exposure.rescale_intensity(hog_image, in_range=(0, 0.02))

##    ax2.axis('off')
##    ax2.imshow(hog_image_rescaled, cmap=plt.cm.gray)
##    ax2.imshow(hog_image, cmap=plt.cm.gray)
##    ax2.set_title('Histogram of Oriented Gradients')
##    plt.show()
    print type(fd);
    return fd;




def get_testsets(cls):
    

    outputFolder = 'C:\\Users\\user\\Documents\\sscet\\me\\thesis\\LPR\outputelm\\'
    

    lpcharnumfile = outputFolder + 'licenseplatetotalcharnum.txt' 

    
    lpcnf = open(lpcharnumfile)
    line = lpcnf.readline()
    lpcn = int(line)
    print(lpcn)
    
    lpcnf.close();
    data=[]
    labels=[]

    for i in range(1, lpcn+1):
        lpcharfilestr = outputFolder + 'licenseplatechar' + str(i) + '.bmp'
        print "trying to read ", lpcharfilestr
        fd = get_hog_data(lpcharfilestr)
        data.append(fd)

    labels = cls.predict(data)

    labels_out=""

    for label in labels:
        
        labels_out = labels_out + chr(label)
    print(labels_out)

    
    
    return labels_out
        

if __name__ == '__main__':

    
    classifier = joblib.load('C:\Users\user\Documents\MATLAB\matlab 7.10.0 (2010)\ANNALPRSMALL\my_elm.pkl')
    #classifier=joblib.load('my_elm.pkl')
    labels_out=get_testsets(classifier)
    outputFileName = 'C:\Users\user\Documents\MATLAB\matlab 7.10.0 (2010)\ANNALPRSMALL\output.txt'
    outputFile = open(outputFileName,'w')
    outputFile.write(labels_out)
    outputFile.close()
    
