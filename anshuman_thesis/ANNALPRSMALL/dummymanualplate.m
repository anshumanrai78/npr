
function dummy(numberplate)

base=load(['' 'trained1.mat']); 
imshow(numberplate)
disp('numberplate')
pause()
f=imread(numberplate);
g=rgb2gray(f); % Converting the RGB (color) image to gray (intensity).
g=medfilt2(g,[3 3]); % Median filtering to remove noise.



g=rgb2gray(f); % Converting the RGB (color) image to gray (intensity).

disp('showing number plate')
imshow(g)

g=medfilt2(g,[3 3]); % Median filtering to remove noise.
se=strel('disk',1); % Structural element (disk of radius 1) for morphological processing.
gi=imdilate(g,se); % Dilating the gray image with the structural element.
ge=imerode(g,se); % Eroding the gray image with structural element.
gdiff=imsubtract(gi,ge); % Morphological Gradient for edges enhancement.
gdiff=mat2gray(gdiff); % Converting the class to double.
gdiff=conv2(gdiff,[1 1;1 1]); % Convolution of the double image for brightening the edges.
gdiff=imadjust(gdiff,[0.5 0.7],[0 1],0.1); % Intensity scaling between the range 0 to 1.
B=logical(gdiff); % Conversion of the class from double to binary.
imshow(B)
disp('before horizontal lines removal')
pause()
% Eliminating the possible horizontal lines from the output image of regiongrow
% that could be edges of license plate.
er=imerode(B,strel('line',50,0));
imshow(er)
disp('after erosion')
pause()
out1=imsubtract(B,er);
imshow(out1)
disp('after horizontal lines removal')
pause()
%elimniate vertical lines
%er=imerode(out1,strel('line',50,90));
%disp('checking vertical lines')
%imshow(er)
F=imfill(out1,'holes');
% Thinning the image to ensure character isolation.
H=bwmorph(F,'thin',1);
H=imerode(H,strel('line',3,90));
% Selecting all the regions that are of pixel area more than 100.
final=bwareaopen(H,10);

imwrite(final, 'final.jpg')
disp('after morphological')


pause()




[L,num] = bwlabel(final); %Find components

disp(num)
figure();   %Create figure
imagesc(L);  %Draw the components, each in its own color.
pause()
%Get the bounding box Rectangle containing the region
Sdata=regionprops(L,'BoundingBox');



for i=1:num
    disp(i)
    heightchar(i) = Sdata(i).BoundingBox(3);
end

disp(heightchar)
pause()

%find mode of height
heightmode=mode(heightchar)
disp('height 1d perim')
disp(heightmode)

%find center of image

center = size(final)/2;

disp(center(1))
disp(center(2))

pause()

%find which connected component is closest to the center

stats = regionprops(L, 'Centroid')

for i=1:num
    s_y(i)=stats(i).Centroid(1)
    s_x(i)=stats(i).Centroid(2)
end


mindist = 50000
mindistindex = 0
for i=1:num
    currdist = pdist2(center, [s_x(i), s_y(i)], 'euclidean')
    dist(i) = currdist
    if currdist < mindist
        mindist = currdist
        mindistindex = i
    end
end

Img = imcrop(final,Sdata(mindistindex).BoundingBox)
imshow(Img)
disp('showing center character')
pause()
imwrite(Img, 'center.jpg')

centercharheight = Sdata(mindistindex).BoundingBox(3)

disp(centercharheight)
disp(heightchar)
pause()

a=[];

%x = find(height<=largestheight+.1)
%disp('number of matching characters')
%disp(length(x))
noPlate=[]
for i=1:num
    currheight = heightchar(i)
    deviation = .1 * centercharheight
    if (currheight <= centercharheight + deviation && currheight >= centercharheight - deviation)
        Img = imcrop(final,Sdata(i).BoundingBox);
        %imwrite(Img, strcat('img',num2str(i),'.jpg'))
        %letter=readLetter(Img)
        imshow(Img);
        %disp(letter)
        %noPlate = [noPlate letter]
        %%M = (L==x(i))
        %disp('enter')
        %pause()
        %imshow(M)
        bw = im2bw(Img,0.5);
        bw2 = edu_imgcrop(bw);
        charvec = edu_imgresize(bw2);
        figure,imshow(bw);
        pause();

        %anshuman : modified 29/04/2015

        %selected_net = evalin('base','net');

        %result = sim(selected_net, charvec);
        result = sim(base.net,charvec);

        a=[a vec2ind(result)];
        
        
    end
    

end

a
display('size(a)');
display(size(a));
[m,n]= size(a);
lnumber=[];

for i=1:n
if  a(i)==1
    lnumber=[lnumber '0'];
end
    

if a(i)==2
    lnumber=[lnumber '1'];
end

if a(i)==3
    lnumber=[lnumber '2'];
end

if a(i)==4
    lnumber=[lnumber '3'];
end

if a(i)==5
    lnumber=[lnumber '4'];
end

if a(i)==6
    lnumber=[lnumber '5'];
end

if a(i)==7
    lnumber=[lnumber '6'];
end

if a(i)==8
    lnumber=[lnumber '7'];
end

if a(i)==9
    lnumber=[lnumber '8'];
end

if a(i)==10
    lnumber=[lnumber '9'];
end

if a(i)==11
    lnumber=[lnumber '0'];
end


if  a(i)==12
    lnumber=[lnumber 'A'];
end

if a(i)==13
    lnumber=[lnumber 'B'];
end

if a(i)==14
    lnumber=[lnumber 'C'];
end

if a(i)==15
    lnumber=[lnumber 'D'];
end

if a(i)==16
    lnumber=[lnumber 'E'];
end

if a(i)==17
    lnumber=[lnumber 'F'];
end

if a(i)==18
    lnumber=[lnumber 'G'];
end

if a(i)==19
    lnumber=[lnumber 'H'];
end

if a(i)==20
    lnumber=[lnumber 'I'];
end

if a(i)==21
    lnumber=[lnumber 'J'];
end

if a(i)==22
    lnumber=[lnumber 'K'];
end

if  a(i)==23
    lnumber=[lnumber 'L'];
end

if a(i)==24
    lnumber=[lnumber 'M'];
end

if a(i)==25
    lnumber=[lnumber 'N'];
end

if a(i)==26
    lnumber=[lnumber 'O'];
end

if a(i)==27
    lnumber=[lnumber 'P'];
end

if a(i)==28
    lnumber=[lnumber 'Q'];
end

if a(i)==29
    lnumber=[lnumber 'R'];
end

if a(i)==30
    lnumber=[lnumber 'S'];
end

if a(i)==31
    lnumber=[lnumber 'T'];
end

if a(i)==32
    lnumber=[lnumber 'U'];
end

if a(i)==33
    lnumber=[lnumber 'V'];
end

if a(i)==34
    lnumber=[lnumber 'W'];
end

if a(i)==35
    lnumber=[lnumber 'X'];
end

if a(i)==36
    lnumber=[lnumber 'Y'];
end

if a(i)==37
    lnumber=[lnumber 'Z'];
end

end



lnumber
fid=fopen('output.txt','w+');
fprintf(fid,'%s\n',lnumber);
fclose(fid);
winopen('output.txt');
return

%fid = fopen('noPlate.txt', 'wt'); % This portion of code writes the number plate
%fprintf(fid,'%s\n',noPlate);      % to the text file, if executed a notepad file with the
%fclose(fid);                      % name noPlate.txt will be open with the number plate written.
%winopen('noPlate.txt')


