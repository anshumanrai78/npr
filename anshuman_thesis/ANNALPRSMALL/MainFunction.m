%% Downloaded from http://berksoysal.blogspot.ca
% Licence Plate Detector Based on canny edge detector algorithm.
function MainFunction(filename)
%clear;clc;close all

% Please enter the image name below 
%filename='3.jpg';
I=im2gray(filename);

tic  

[height,width]=size(I);

I_edge=zeros(height,width);
for i=1:width-1
    I_edge(:,i)=abs(I(:,i+1)-I(:,i));
end

I_edge=(255/(max(max(I_edge))-min(min(I_edge))))*(I_edge-min(min(I_edge)));

[I_edge,y1]=select(I_edge,height,width);   

BW2 = I_edge;


SE=strel('rectangle',[10,10]);
IM2=imerode(BW2,SE);
IM2=bwareaopen(IM2,20);
IM3=imdilate(IM2,SE);


%%%%%%%%%%%%%%%%
p_h=projection(double(IM3),'h');             
if(p_h(1)>0)
    p_h=[0,p_h];
end
p_v=projection(double(IM3),'v');                
if(p_v(1)>0)
    p_v=[0,p_v];
end

%%%%%%
p_h=double((p_h>5));
p_h=find(((p_h(1:end-1)-p_h(2:end))~=0));
len_h=length(p_h)/2;
%%%%%
p_v=double((p_v>5));
p_v=find(((p_v(1:end-1)-p_v(2:end))~=0));
len_v=length(p_v)/2;
%%%%%%%%%%%


%%%%%%%%%%%%%%%
k=1;
for i=1:len_h
    for j=1:len_v
        s=IM3(p_h(2*i-1):p_h(2*i),p_v(2*j-1):p_v(2*j));
        if(mean(mean(s))>0.1)
            p{k}=[p_h(2*i-1),p_h(2*i)+1,p_v(2*j-1),p_v(2*j)+1];
            k=k+1;
        end
    end
end
k=k-1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i=1:k
   edge_IM3=double(edge(double(IM3(p{i}(1):p{i}(2),p{i}(3):p{i}(4))),'canny'));

   [x,y]=find(edge_IM3==1);
   p{i}=[p{i}(1)+min(x),p{i}(2)-(p{i}(2)-p{i}(1)+1-max(x)),...
         p{i}(3)+min(y),p{i}(4)-(p{i}(4)-p{i}(3)+1-max(y))];
   p_center{i}=[fix((p{i}(1)+p{i}(2))/2),fix((p{i}(3)+p{i}(4))/2)];
   p_ratio(i)=(p{i}(4)-p{i}(3))/(p{i}(2)-p{i}(1));
end

if k>1
    n=0;
    ncount=zeros(1,k);
    for i=1:k-1
       
        if(abs(p{i}(1)+p{i}(2)-p{i+1}(1)-p{i+1}(2))<=height/30&&abs(p{i+1}(3)-p{i}(4))<=width/15)
            p{i+1}(1)=min(p{i}(1),p{i+1}(1));
            p{i+1}(2)=max(p{i}(2),p{i+1}(2));
            p{i+1}(3)=min(p{i}(3),p{i+1}(3));
            p{i+1}(4)=max(p{i}(4),p{i+1}(4));  
            n=n+1;
            ncount(n)=i+1;
        end
    end
    
    if(n>0)
        d_ncount=ncount(2:n+1)-ncount(1:n);
        index=find(d_ncount~=1);
        m=length(index);
        for i=1:m
            pp{i}=p{ncount(index(i))};
            
            
            pp_ratio(i)=(pp{i}(4)-pp{i}(3))/(pp{i}(2)-pp{i}(1));     
        end
        p=pp;
        p_ratio=pp_ratio; 
        clear pp;clear pp_ratio;
    end
end
k=length(p); 

m=1;T=0.6*max(p_ratio);
for i=1:k
    if(p_ratio(i)>=T&p_ratio(i)<20)
        p1{m}=p{i};
        m=m+1;
    end
end
p=p1;clear p1;
k=m-1;  

toc                                

clear edge_IM3;clear x; clear y;     
%% OUTPUT

figure;
subplot(221);imshow(I);
subplot(222);imshow(BW2);
subplot(223);imshow(IM2);
subplot(224);imshow(IM3);


figure;
for i=1:k
    subplot(1,k,i);
    index=p{i};
    imshow(I(index(1)-2:index(2),index(3):index(4)));
end
if(k==1)
    imwrite(I(index(1)-2:index(2),index(3):index(4)),'licenceplate.jpg');
end
end