function varargout = LPR(varargin)
% LPR M-file for LPR.fig
%      LPR, by itself, creates a new LPR or raises the existing
%      singleton*.
%
%      H = LPR returns the handle to a new LPR or the handle to
%      the existing singleton*.
%
%      LPR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LPR.M with the given input arguments.
%
%      LPR('Property','Value',...) creates a new LPR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before LPR_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to LPR_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help LPR

% Last Modified by GUIDE v2.5 27-Jun-2015 12:47:33

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @LPR_OpeningFcn, ...
                   'gui_OutputFcn',  @LPR_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

end

% --- Executes just before LPR is made visible.
function LPR_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to LPR (see VARARGIN)

% Choose default command line output for LPR
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes LPR wait for user response (see UIRESUME)
% uiwait(handles.NetworkType);
str{1}='LVQ'
str{2}='ELM'
str{3}='DBN'
set(handles.listbox1, 'String', str) 
%set(handles.listbox1, 'String', cellstr(num2str((1:5).', 'House %d')) );
nntype = get(handles.listbox1, 'value');
fid = fopen('nntype.txt', 'wt');
fprintf(fid,'%d',nntype);      
fclose(fid);
end

% --- Outputs from this function are returned to the command line.
function varargout = LPR_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

end

% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
disp('Load image button pressed ...')
%% Create a file dialog for images
[filename, user_cancelled] = imgetfile;
    if user_cancelled
            disp('User pressed cancel')
    else
            disp(['User selected ', filename])
    end

    %% Read the selected image into the variable
    disp('Reading the image into variable X');
    [X,map] = imread(filename);
    
    %% Copy X and map to base workspace, overwriting the content !!!
    assignin('base','X',X); 
    assignin('base','map',map); 
    %% Now you have X and map variables in the base workspace
    display('calling demo with filename');
    display(filename);
    pause();
    dummydemolvq(filename)
    
end


% --- Executes on key press with focus on pushbutton1 and none of its controls.
function pushbutton1_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)

end

% --- Executes on button press in pushbuttonloadtrainimg.
function pushbuttonloadtrainimg_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonloadtrainimg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    nntype = get(handles.listbox1, 'value');
    display(nntype)
    if (nntype == 1)
        [lvqfilename, user_cancelled] = imgetfile;
        if user_cancelled
            disp('User pressed cancel')
        else
            disp(['User selected ', lvqfilename])
        end
        display(lvqfilename)
        fid = fopen('lvqtrainingfilename.txt', 'wt');
        fprintf(fid,'%s',lvqfilename);      
        fclose(fid);
    else
        if (nntype == 2)
            elmdirname = uigetdir;
            display( elmdirname)
            fid = fopen('elmtrainingdirname.txt', 'wt');
            fprintf(fid,'%s',elmdirname);      
            fclose(fid);
        else 
            if (nntype == 3)
                dbndirname = uigetdir;
                display(dbndirname)
                fid = fopen('dbntrainingdirname.txt', 'wt');
                fprintf(fid,'%s',dbndirname);      
                fclose(fid);
            end
        end
    end
end

% --- Executes on button press in pushbuttontrainnn.
function pushbuttontrainnn_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttontrainnn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    nntype = get(handles.listbox1, 'value')
    display(nntype)
    if (nntype == 1)
        char3()
    else
        if (nntype == 2)
            fid = fopen('elmtrainingdirname.txt', 'r');
            elmdirname = fscanf(fid, '%c');      
            fclose(fid);
            display(elmdirname);
            elmscript = 'C:\\Users\\user\\Documents\\sscet\\me\\thesis\\LPR\\related\\elm\\adam\\elm_script.py '
            trainelmstr = [elmscript '"' elmdirname '"'];
            display(trainelmstr)
            s = system(trainelmstr)
        else 
            if (nntype == 3)
                fid = fopen('dbntrainingdirname.txt', 'r');
                dbndirname = fscanf(fid, '%c');
                fclose(fid);
                display(dbndirname);
                dbnscript = 'C:\\Users\\user\\Documents\\sscet\\me\\thesis\\LPR\\related\\dbn\\mydbn-from-file.py '
                traindbnstr = [dbnscript '"' dbndirname '"'];
                display(traindbnstr)
                s = system(traindbnstr)
            end
        end
    end
end

% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    nntype = get(handles.listbox1, 'value');
    fid = fopen('nntype.txt', 'wt');
    fprintf(fid,'%d',nntype);      
    fclose(fid);
end
% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

end
