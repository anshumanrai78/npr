# import the necessary packages
from sklearn.cross_validation import train_test_split
from sklearn.metrics import classification_report
from sklearn import datasets
from nolearn.dbn import DBN
import numpy as np
import cv2
import scipy.io
import sys
import cv2
import Image
import os
import glob
from sklearn.externals import joblib
from skimage.feature import hog
from skimage import data, color, exposure


from skimage.morphology import medial_axis
import matplotlib.pyplot as plt
from skimage import morphology
from skimage.morphology import skeletonize


STANDARD_SIZE=(32,32)


    




def get_one_img(cls):
    f, v= get_features_data('caffee\\caffe-ocr-master\\training_images\\A\\Verdana-8-3.png')
    return f, v

def get_feature_data(imagefullpathname):
    im=Image.open(imagefullpathname)
    
    im=im.convert('L');
    im = im.resize(STANDARD_SIZE)
    im=np.asarray(im)
    im=im/255.0
    #im=im.astype('int0')
    image = np.reshape(im,[28*28, ])
    return image , image

def get_features_data(imagefullpathname):
    
    image = Image.open(imagefullpathname)
    #image = trim(image)
    image=image.convert('L');
    image = image.resize(STANDARD_SIZE)
    image = np.asarray(image)
    #image = np.reshape(image, [32*32], 1)
    #return (image, image)

    #image = cv2.threshold(image, 0, 255, cv2.THRESH_OTSU)[1]
    #skel = morphology.skeletonize(image > 0)

##    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4))
##    ax1.imshow(image, cmap=plt.cm.gray, interpolation='nearest')
##    ax1.axis('off')
##    ax2.imshow(skel, cmap=plt.cm.spectral, interpolation='nearest')
##    ax2.contour(image, [0.5], colors='w')
##    ax2.axis('off')
##
##    fig.subplots_adjust(hspace=0.01, wspace=0.01, top=1, bottom=0, left=0, right=1)
##    plt.show()

    

    fd, hog_image = hog(image, orientations=8, pixels_per_cell=(4, 4),
        cells_per_block=(1, 1), visualise=True)
##    fd, hog_image = hog(image, orientations=8, pixels_per_cell=(16, 16),
##        cells_per_block=(1, 1), visualise=True)

##    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4))
##
##    ax1.axis('off')
##    ax1.imshow(image, cmap=plt.cm.gray)
##    ax1.set_title('Input image')

    # Rescale histogram for better display
##    hog_image_rescaled = exposure.rescale_intensity(hog_image, in_range=(0, 0.02))
##
##    ax2.axis('off')
##    ax2.imshow(hog_image_rescaled, cmap=plt.cm.gray)
##    ax2.imshow(hog_image, cmap=plt.cm.gray)
##    ax2.set_title('Histogram of Oriented Gradients')
##    plt.show()
    print type(fd);
    return fd, hog_image;


def get_testsets(cls):
    

    outputFolder = 'C:\\Users\\user\\Documents\\sscet\\me\\thesis\\LPR\outputdbn\\'
    

    lpcharnumfile = outputFolder + 'licenseplatetotalcharnum.txt' 

    
    lpcnf = open(lpcharnumfile)
    line = lpcnf.readline()
    lpcn = int(line)
    print(lpcn)
    
    lpcnf.close();
    data=np.asarray([])

    for i in range(1, lpcn+1):
        lpcharfilestr = outputFolder + 'licenseplatechar' + str(i) + '.bmp'
        
        print "trying to read ", lpcharfilestr
        fd, vis = get_features_data(lpcharfilestr)
        if (i==1):
                data = fd
                print 'concatenated'
                print data.shape
        else:
                print 'concatenating'
                print fd.shape
                print data.shape
                data = np.vstack((data, fd))
                print 'after concat'
                print data.shape

    print 'predicting'
    return data
    
        

if __name__ == '__main__':

    dbn = joblib.load('C:\Users\user\Documents\MATLAB\matlab 7.10.0 (2010)\ANNALPRSMALL\my_dbn.pkl')
    #bn = joblib.load('my_dbn.pkl')
    data = get_testsets(dbn)
    labels = dbn.predict(data)
    print 'done prediction'
    labels_out=""
    for label in labels:
        if (label >= 0 and label <= 9):
            label_ord = label + ord('0')
        else:
            label_ord = label + ord('A') - 10
        labels_out = labels_out + chr(label_ord)
    print(labels_out)
    print 'wahoo'
    outputFileName = 'C:\Users\user\Documents\MATLAB\matlab 7.10.0 (2010)\ANNALPRSMALL\output.txt'
    outputFile = open(outputFileName,'w')
    outputFile.write(labels_out)
    outputFile.close()
    #img = get_one_img(dbn)
    #pred = dbn.predict(np.atleast_2d(img))
    
    

