
function dummydemolvq(fstr)

fid = fopen('nntype.txt', 'r');
nntype = fscanf(fid, '%d');      
fclose(fid);

base=load(['' 'trained1.mat']);

fimg = imread(fstr);
figure('name', 'Original Image');
imshow(fimg)
disp('original image')
pause()

Demo(fstr);

outputFolderelm = 'C:\Users\user\Documents\sscet\me\thesis\LPR\outputelm\'; % or frames instead of test.
outputFolderdbn = 'C:\Users\user\Documents\sscet\me\thesis\LPR\outputdbn\'; % or frames instead of test.


%Now from the number plate we apply morphological operations



    figure('name', 'Morphological Operations');
    numberplate = imread('licenceplate.jpg');
    g = numberplate;
    %g=rgb2gray(numberplate); % Converting the RGB (color) image to gray (intensity).

    disp('showing number plate')
    %imshow(g)
    subplot(4,4,1);subimage(g);
    
    
    %contrast the license plate
    
    display('original  license plate image');
    imshow(g);
    %pause();
    gsize = size(g)
    display(gsize);
    gh = gsize(1);
    display(gh);
    display(round(gh/2));
    %pause();
    st1 = strel('disk', round(gh/2));
    tophat_img = imtophat(g, st1);
    bottomhat_img = imbothat(g, st1);
    g = g + tophat_img - bottomhat_img;
    subplot(4,4,2);subimage(g);
    display(g);
    display('contrast license plate image');
    %pause();

    g=medfilt2(g,[3 3]); % Median filtering to remove noise.
    subplot(4,4,3);subimage(g);
    se=strel('disk',1); % Structural element (disk of radius 1) for morphological processing.
    gi=imdilate(g,se); % Dilating the gray image with the structural element.
    subplot(4,4,4);subimage(gi);
    ge=imerode(g,se); % Eroding the gray image with structural element.
    subplot(4,4,5);subimage(ge);
    gdiff=imsubtract(gi,ge); % Morphological Gradient for edges enhancement.
    subplot(4,4,6);subimage(gi);
    gdiff=mat2gray(gdiff); % Converting the class to double.
    subplot(4,4,7);subimage(gdiff);
    gdiff=conv2(gdiff,[1 1;1 1]); % Convolution of the double image for brightening the edges.
    subplot(4,4,8);subimage(gdiff);
    gdiff=imadjust(gdiff,[0.5 0.7],[0 1],0.1); % Intensity scaling between the range 0 to 1.
    subplot(4,4,9);subimage(gdiff);
    B=logical(gdiff); % Conversion of the class from double to binary.
    subplot(4,4,10);subimage(B);
    %imshow(B)
    disp('before horizontal lines removal')
    %pause()
    % Eliminating the possible horizontal lines from the output image of regiongrow
    % that could be edges of license plate.
    er=imerode(B,strel('line',50,0));
    subplot(4,4,11);subimage(er);
    %imshow(er)
    disp('after erosion')
    %pause()
    out1=imsubtract(B,er);
    %imshow(out1)
    subplot(4,4,12);subimage(out1);
    disp('after horizontal lines removal')
    %pause()
    %elimniate vertical lines
    %er=imerode(out1,strel('line',50,90));
    %disp('checking vertical lines')
    %imshow(er)
    F=imfill(out1,'holes');
    subplot(4,4,13);subimage(F);
    % Thinning the image to ensure character isolation.
    H=bwmorph(F,'thin',1);
    H=imerode(H,strel('line',3,90));
    subplot(4,4,14);subimage(H);
    % Selecting all the regions that are of pixel area more than 100.
    final=bwareaopen(H,10);
    subplot(4,4,15);subimage(final);
    imwrite(final, 'final.jpg')
    disp('after morphological')
    %imshow(final);
    
    pause();
    
    
    


    %pause()




    [L,num] = bwlabel(final); %Find components

    disp(num)
    figure('name', 'Connected Component Labeling');   %Create figure
    imagesc(L);  %Draw the components, each in its own color.
    pause()
    %Get the bounding box Rectangle containing the region
    Sdata=regionprops(L,'BoundingBox');



    for i=1:num
        disp(i)
        heightchar(i) = Sdata(i).BoundingBox(4);
    end

    disp(heightchar)
    %pause()

    %find mode of height
    heightmode=mode(heightchar)
    disp('height 1d perim')
    disp(heightmode)

    %find center of image

    center = size(final)/2;

    disp(center(1))
    disp(center(2))

    %pause()

    %find which connected component is closest to the center

    stats = regionprops(L, 'Centroid')

    for i=1:num
        s_y(i)=stats(i).Centroid(1)
        s_x(i)=stats(i).Centroid(2)
    end


    mindist = 50000
    mindistindex = 0
    for i=1:num
        currdist = pdist2(center, [s_x(i), s_y(i)], 'euclidean')
        dist(i) = currdist
        if currdist < mindist
            mindist = currdist
            mindistindex = i
        end
    end

    ccfig = figure('name', 'Central Character')
    set(ccfig, 'Position', [0 0 200 500]);
    Img = imcrop(final,Sdata(mindistindex).BoundingBox)
    imshow(Img)
    disp('showing center character')
    pause()
    imwrite(Img, 'center.jpg')

    centercharheight = Sdata(mindistindex).BoundingBox(4)

    disp(centercharheight)
    disp(heightchar)
    %pause()

    a=[];

    %x = find(height<=largestheight+.1)
    %disp('number of matching characters')
    %disp(length(x))
    noPlate=[]
    j=0;
    figure('name', 'Character Segmentation');
    for i=1:num
        currheight = heightchar(i)
        deviation = .1 * centercharheight
        if (currheight <= centercharheight + deviation && currheight >= centercharheight - deviation)
            Imgdbn = imcrop(numberplate,Sdata(i).BoundingBox);
            Imgelm = imcrop(final,Sdata(i).BoundingBox);

            %imwrite(Img, strcat('img',num2str(i),'.jpg'))
            %letter=readLetter(Img)
            
            %disp(letter)
            %noPlate = [noPlate letter]
            %%M = (L==x(i))
            %disp('enter')
            %pause()
            %imshow(M)
            
            Imgdbnbw = im2bw(Imgdbn,0.5);
            Imgdbnbw2 = edu_imgcrop(Imgdbnbw);
            Imgelmbw = im2bw(Imgelm,0.5);
            Imgelmbw2 = edu_imgcrop(Imgelmbw);
            %charvec = edu_imgresize(bw2);
            %charvec = feature_extractor(bw2);
            %charvec = bw2;
            %figure,imshow(bw2);
            
            j=j+1;
            licenseplatechardbn = strcat(outputFolderdbn, 'licenseplatechar' , int2str(j), '.bmp');
            imwrite(Imgdbnbw2, licenseplatechardbn);
            licenseplatecharelm = strcat(outputFolderelm, 'licenseplatechar' , int2str(j), '.bmp');
            imwrite(Imgelmbw2, licenseplatecharelm);
            %pause();
             
            %anshuman : modified 29/04/2015
            
            bw = im2bw(Imgelm,0.5);
            bw2 = edu_imgcrop(bw);
            charvec = edu_imgresize(bw2);
            %charvec = feature_extractor(bw2);
            %charvec = bw2;
            subplot(4,4,j),subimage(bw2);
            %pause();
             %figure, imshow(charvec);
             %pause();
            %anshuman : modified 29/04/2015

            %selected_net = evalin('base','net');

            %result = sim(selected_net, charvec);
            result = sim(base.net,charvec);
%             display('result');
%             display(result);
%             pause();
%             display('vec2ind');
%             display(vec2ind(result));
%             pause();
            a=[a vec2ind(result)];
            


        end


    end
    
    pause();
    display(nntype)
    pause();
    if (nntype == 1)
        
        a
        display('size(a)');
        display(size(a));
        [m,n]= size(a);
        lnumber=[];
        
        for i=1:n
            if  a(i)==1
                lnumber=[lnumber '0'];
            end
            
            
            if a(i)==2
                lnumber=[lnumber '1'];
            end
            
            if a(i)==3
                lnumber=[lnumber '2'];
            end
            
            if a(i)==4
                lnumber=[lnumber '3'];
            end
            
            if a(i)==5
                lnumber=[lnumber '4'];
            end
            
            if a(i)==6
                lnumber=[lnumber '5'];
            end
            
            if a(i)==7
                lnumber=[lnumber '6'];
            end
            
            if a(i)==8
                lnumber=[lnumber '7'];
            end
            
            if a(i)==9
                lnumber=[lnumber '8'];
            end
            
            if a(i)==10
                lnumber=[lnumber '9'];
            end
            
            
            
            if  a(i)==11
                lnumber=[lnumber 'A'];
            end
            
            if a(i)==12
                lnumber=[lnumber 'B'];
            end
            
            if a(i)==13
                lnumber=[lnumber 'C'];
            end
            
            if a(i)==14
                lnumber=[lnumber 'D'];
            end
            
            if a(i)==15
                lnumber=[lnumber 'E'];
            end
            
            if a(i)==16
                lnumber=[lnumber 'F'];
            end
            
            if a(i)==17
                lnumber=[lnumber 'G'];
            end
            
            if a(i)==18
                lnumber=[lnumber 'H'];
            end
            
            if a(i)==19
                lnumber=[lnumber 'I'];
            end
            
            if a(i)==20
                lnumber=[lnumber 'J'];
            end
            
            if a(i)==21
                lnumber=[lnumber 'K'];
            end
            
            if  a(i)==22
                lnumber=[lnumber 'L'];
            end
            
            if a(i)==23
                lnumber=[lnumber 'M'];
            end
            
            if a(i)==24
                lnumber=[lnumber 'N'];
            end
            
            if a(i)==25
                lnumber=[lnumber 'O'];
            end
            
            if a(i)==26
                lnumber=[lnumber 'P'];
            end
            
            if a(i)==27
                lnumber=[lnumber 'Q'];
            end
            
            if a(i)==28
                lnumber=[lnumber 'R'];
            end
            
            if a(i)==29
                lnumber=[lnumber 'S'];
            end
            
            if a(i)==30
                lnumber=[lnumber 'T'];
            end
            
            if a(i)==31
                lnumber=[lnumber 'U'];
            end
            
            if a(i)==32
                lnumber=[lnumber 'V'];
            end
            
            if a(i)==33
                lnumber=[lnumber 'W'];
            end
            
            if a(i)==34
                lnumber=[lnumber 'X'];
            end
            
            if a(i)==35
                lnumber=[lnumber 'Y'];
            end
            
            if a(i)==36
                lnumber=[lnumber 'Z'];
            end
            
        end

    

        lnumber
        fid=fopen('output.txt','w+');
        fprintf(fid,'%s\n',lnumber);
        fclose(fid);
        winopen('output.txt');
    end
    
    
    lprcharnumfilestr = strcat(outputFolderelm, 'licenseplatetotalcharnum.txt');
    lprcharnumfileID = fopen(lprcharnumfilestr,'w');
    fprintf(lprcharnumfileID,'%d\n',j);
    fclose(lprcharnumfileID);
    
    lprcharnumfilestr = strcat(outputFolderdbn, 'licenseplatetotalcharnum.txt');
    lprcharnumfileID = fopen(lprcharnumfilestr,'w');
    fprintf(lprcharnumfileID,'%d\n',j);
    fclose(lprcharnumfileID);

    if (nntype == 2)
        elmscript = 'C:\\Users\\user\\Documents\\sscet\\me\\thesis\\LPR\\related\\elm\\adam\\elm_script_load.py '
        s = system(elmscript)
        winopen('output.txt');
    end

    if (nntype == 3)
        dbnscript = 'C:\\Users\\user\\Documents\\sscet\\me\\thesis\\LPR\\related\\dbn\\mydbn-load-from-file.py '
        s = system(dbnscript)
        winopen('output.txt');
    end
    %lnumber
    %fid=fopen('output.txt','w+');
    %fprintf(fid,'%s\n',lnumber);
    %fclose(fid);
    %winopen('output.txt');
    

    %fid = fopen('noPlate.txt', 'wt'); % This portion of code writes the number plate
    %fprintf(fid,'%s\n',noPlate);      % to the text file, if executed a notepad file with the
    %fclose(fid);                      % name noPlate.txt will be open with the number plate written.
    %winopen('noPlate.txt')
end



