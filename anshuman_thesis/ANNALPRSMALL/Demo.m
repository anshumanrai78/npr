% This code implemented license plate detection using morphological 
% operators and loosely follows the approach presented by:
% 1. Farhad Faradji, Amir Hossein Rezaie, Majid Ziaratban �a morphological
% based license plate location� ICIP 2007.
% 2. Farhad Faraji, and Reza Safabakhsh, �novel and fast method for 
% detecting plate location from complicated images based on morphological 
% operations� MVIP 2007.

% Alireza Asvadi
% Department of ECE, SPR Lab
% Babol (Noshirvani) University of Technology
% http://www.a-asvadi.ir
% 2012
%% clear command windows

function Demo(fstr)
%clc
%clear all
%close all
figure('name','License Plate Localization');
%% Read Image
display(fstr);
Im    = imread(fstr);
I     = im2double(rgb2gray(Im));
% figure();imshow(I)
%% Sobel Masking 
SM    = [-1 0 1;-2 0 2;-1 0 1];         % Sobel Vertical Mask
IS    = imfilter(I,SM,'replicate');     % Filter Image Using Sobel Mask
subplot(4,4, 1);subimage(IS);
%imshow(IS);pause();imwrite(IS,'pd1.jpg');
IS    = IS.^2;                          % Consider Just Value of Edges & Fray Weak Edges
% figure();imshow(IS)
%% Normalization
IS    = (IS-min(IS(:)))/(max(IS(:))-min(IS(:))); % Normalization

subplot(4,4,2);subimage(IS);
%imshow(IS);pause();imwrite(IS,'pd2.jpg');
% figure();imshow(IS)
%% Threshold (Otsu)
level = graythresh(IS);                 % Threshold Based on Otsu Method
IS    = im2bw(IS,level);
subplot(4,4,3);subimage(IS);

%imshow(IS);pause();imwrite(IS,'pd3.jpg');
% figure();imshow(IS)
%% Histogram
S     = sum(IS,2);                      % Edge Horizontal Histogram
%imshow(S);pause();plot(1:size(S,1),S)
subplot(4,4, 4);plot(1:size(S,1),S)

%figure();plot(1:size(S,1),S)
%view(90,90);
%pause();
%%Plot
%figure()
%subplot(1,2,1);imshow(IS)
%subplot(1,2,2);plot(1:size(S,1),S)
%axis([1 size(IS,1) 0 max(S)]);view(90,90)
%% Plate Location
T1    = 0.35;                           % Threshold On Edge Histogram
PR    = find(S > (T1*max(S)));          % Candidate Plate Rows
subplot(4,4, 5);subimage(PR);

%imshow(PR);pause();imwrite(PR,'pd5.jpg');
%% Masked Plate
Msk   = zeros(size(I));
Msk(PR,:) = 1;                          % Mask
MB    = Msk.*IS;                        % Candidate Plate (Edge Image)
subplot(4,4, 6);subimage(MB);

%imshow(MB);pause();imwrite(MB,'pd6.jpg');
% figure();imshow(MB)
%% Morphology (Dilation - Vertical)
Dy    = strel('rectangle',[80,4]);      % Vertical Extension
MBy   = imdilate(MB,Dy);                % By Dilation
subplot(4,4, 7);subimage(MBy);

%imshow(MBy);pause();imwrite(MBy,'pd7.jpg');
MBy   = imfill(MBy,'holes');            % Fill Holes
subplot(4,4, 8);subimage(MBy);

%imshow(MBy);pause();imwrite(MBy,'pd8.jpg');
% figure();imshow(MBx)
%% Morphology (Dilation - Horizontal)
Dx    = strel('rectangle',[4,80]);      % Horizontal Extension
MBx   = imdilate(MB,Dx);                % By Dilation
subplot(4,4, 9);subimage(MBx);

%imshow(MBx);pause();imwrite(MBx,'pd9.jpg');
MBx   = imfill(MBx,'holes');            % Fill Holes
subplot(4,4, 10);subimage(MBx);

%imshow(MBx);pause();imwrite(MBx,'pd10.jpg');
% figure();imshow(MBy)
%% Joint Places
BIM   = MBx.*MBy;                       % Joint Places
subplot(4,4, 11);subimage(BIM);

%imshow(BIM);pause();imwrite(BIM,'pd11.jpg');
% figure();imshow(BIM)
%% Morphology (Dilation - Horizontal)
Dy    = strel('rectangle',[4,30]);      % Horizontal Extension
MM    = imdilate(BIM,Dy);               % By Dilations
subplot(4,4, 12);subimage(MM);

%imshow(MM);pause();imwrite(MM,'pd12.jpg');
MM    = imfill(MM,'holes');             % Fill Holes
subplot(4,4, 13);subimage(MM);
%imshow(MM);pause();imwrite(MM,'pd13.jpg');
% figure();imshow(MM)
%% Erosion
Dr    = strel('line',50,0);             % Erosion
BL    = imerode(MM,Dr);
subplot(4,4, 14);subimage(BL);

%imshow(BL);pause();imwrite(BL,'pd14.jpg');
% figure();imshow(BL)
%% Find Biggest Binary Region (As a Plate Place)
[L,num] = bwlabel(BL);                  % Label (Binary Regions)
%figure();
subplot(4,4, 15);
imagesc(L);  %Draw the components, each in its own color.
Areas   = zeros(num,1);
for i = 1:num                           % Compute Area Of Every Region
[r,c,v]  = find(L == i);                % Find Indexes
Areas(i) = sum(v);                      % Compute Area    
end 
[La,Lb] = find(Areas==max(Areas));      % Biggest Binary Region Index
%% Post Processing
[a,b]   = find(L==La);                  % Find Biggest Binary Region (Plate)
[nRow,nCol] = size(I);
FM      = zeros(nRow,nCol);             % Smooth and Enlarge Plate Place
T       = 10;                           % Extend Plate Region By T Pixel
jr      = (min(a)-T :max(a)+T);
jc      = (min(b)-T :max(b)+T);
jr      = jr(jr >= 1 & jr <= nRow);
jc      = jc(jc >= 1 & jc <= nCol);
FM(jr,jc) = 1; 
PL      = FM.*I;                        % Detected Plate
subplot(4,4, 16);subimage(PL);

%imshow(PL);imwrite(PL,'pd16.jpg');
% figure();imshow(FM)
% figure();imshow(PL)
%% Plot
imshow(Im); title('Detected Plate')
hold on
h=rectangle('Position',[min(jc),min(jr),max(jc)-min(jc),...
max(jr)-min(jr)],'LineWidth',4,'EdgeColor','r')

hold off
x = min(jc);
y = min(jr);
w = max(jc) - min(jc);
h = max(jr) - min(jr);



licenceplate = Im(round(y:y+h),round(x:x+w));




imshow(licenceplate);
pause();
imwrite(licenceplate, 'licenceplate.jpg');

end