function varargout = LPR(varargin)
% LPR M-file for LPR.fig
%      LPR, by itself, creates a new LPR or raises the existing
%      singleton*.
%
%      H = LPR returns the handle to a new LPR or the handle to
%      the existing singleton*.
%
%      LPR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LPR.M with the given input arguments.
%
%      LPR('Property','Value',...) creates a new LPR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before LPR_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to LPR_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help LPR

% Last Modified by GUIDE v2.5 11-Feb-2015 16:11:08

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @LPR_OpeningFcn, ...
                   'gui_OutputFcn',  @LPR_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before LPR is made visible.
function LPR_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to LPR (see VARARGIN)

% Choose default command line output for LPR
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes LPR wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = LPR_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
disp('Load image button pressed ...')
%% Create a file dialog for images
[filename, user_cancelled] = imgetfile;
    if user_cancelled
            disp('User pressed cancel')
    else
            disp(['User selected ', filename])
    end

    %% Read the selected image into the variable
    disp('Reading the image into variable X');
    [X,map] = imread(filename);
    %% Copy X and map to base workspace, overwriting the content !!!
    assignin('base','X',X); 
    assignin('base','map',map); 
    %% Now you have X and map variables in the base workspace
    dummy(X)


% --- Executes on key press with focus on pushbutton1 and none of its controls.
function pushbutton1_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
