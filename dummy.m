
function dummy(f)

imshow(f)
disp('original image')
pause()

g=rgb2gray(f); % Converting the RGB (color) image to gray (intensity).
g=medfilt2(g,[3 3]); % Median filtering to remove noise.


%First we extract the number plate

edged = edge(g,'canny',0.5)

imshow(edged)
imwrite(edged, 'edged.jpg')
disp('after edge')

pause()
se=strel('disk',10);
closed = imclose(edged, se)


imshow(closed)
imwrite(closed, 'closed.jpg')
disp('after close')
pause()

filled=imfill(closed,'holes');
imshow(filled)
disp('after filled')
imwrite(filled, 'filled.jpg')
pause()

opened = imopen(filled, se)
imshow(opened)
disp('after open')
imwrite(opened, 'opened.jpg')

pause()


[L1,num1] = bwlabel(opened); %Find components

disp(num1)
figure();   %Create figure
imagesc(L1);  %Draw the components, each in its own color.

%Get the bounding box Rectangle containing the region
Sdata1=regionprops(L1,'BoundingBox');



for i=1:num1
area1(i) = bwarea(L1==i);
end

areasorted1=sort(area1)
disp('sorted 1d area')
disp(areasorted1)

%find largest area - and that is the license plate

largestarea = areasorted1(num1)

disp(largestarea)
x1 = find(area1==largestarea)
disp('number of matching characters')
disp(length(x1))
for i=1:length(x1)
    numberplate = imcrop(f,Sdata1(x1(i)).BoundingBox)
    imshow(numberplate)
    %M = (L==x(i))
    disp('license plate')
    imwrite(numberplate, 'numberplate.jpg')
    pause()
    %imshow(M)
    

end




%Now from the number plate we apply morphological operations


%now we extract the characters from the input number plate image.

%f=imread(fname); % Reading the number plate image.
f=imresize(numberplate,[400 NaN]); % Resizing the image keeping aspect ratio same.

g=medfilt2(g,[3 3]); % Median filtering to remove noise.
se=strel('disk',1); % Structural element (disk of radius 1) for morphological processing.
gi=imdilate(g,se); % Dilating the gray image with the structural element.
ge=imerode(g,se); % Eroding the gray image with structural element.
gdiff=imsubtract(gi,ge); % Morphological Gradient for edges enhancement.
gdiff=mat2gray(gdiff); % Converting the class to double.
gdiff=conv2(gdiff,[1 1;1 1]); % Convolution of the double image for brightening the edges.
gdiff=imadjust(gdiff,[0.5 0.7],[0 1],0.1); % Intensity scaling between the range 0 to 1.
B=logical(gdiff); % Conversion of the class from double to binary.
imshow(B)
disp('before horizontal lines removal')
pause()
% Eliminating the possible horizontal lines from the output image of regiongrow
% that could be edges of license plate.
er=imerode(B,strel('line',50,0));
imshow(er)
disp('after erosion')
pause()
out1=imsubtract(B,er);
imshow(out1)
disp('after horizontal lines removal')
pause()
%elimniate vertical lines
er=imerode(out1,strel('line',50,90));
disp('checking vertical lines')
imshow(er)
F=imfill(out1,'holes');
% Thinning the image to ensure character isolation.
H=bwmorph(F,'thin',1);
H=imerode(H,strel('line',3,90));
% Selecting all the regions that are of pixel area more than 100.
final=bwareaopen(H,10);

imwrite(final, 'final.jpg')
disp('after morphological')


pause()




[L,num] = bwlabel(final); %Find components

disp(num)
figure();   %Create figure
imagesc(L);  %Draw the components, each in its own color.
pause()
%Get the bounding box Rectangle containing the region
Sdata=regionprops(L,'BoundingBox');



for i=1:num
    disp(i)
    heightchar(i) = Sdata(i).BoundingBox(3);
end

disp(heightchar)
pause()

%find mode of height
heightmode=mode(heightchar)
disp('height 1d perim')
disp(heightmode)

%find center of image

center = size(final)/2;

disp(center(1))
disp(center(2))

pause()

%find which connected component is closest to the center

stats = regionprops(L, 'Centroid')

for i=1:num
    s_y(i)=stats(i).Centroid(1)
    s_x(i)=stats(i).Centroid(2)
end


mindist = 50000
mindistindex = 0
for i=1:num
    currdist = pdist2(center, [s_x(i), s_y(i)], 'euclidean')
    dist(i) = currdist
    if currdist < mindist
        mindist = currdist
        mindistindex = i
    end
end

Img = imcrop(final,Sdata(mindistindex).BoundingBox)
imshow(Img)
disp('showing center character')
pause()

centercharheight = Sdata(mindistindex).BoundingBox(3)

disp(centercharheight)
disp(heightchar)
pause()



%x = find(height<=largestheight+.1)
%disp('number of matching characters')
%disp(length(x))
for i=1:num
    currheight = heightchar(i)
    deviation = .1 * centercharheight
    if (currheight <= centercharheight + deviation && currheight >= centercharheight - deviation)
        Img = imcrop(final,Sdata(i).BoundingBox);
        %imwrite(Img, strcat('img',num2str(i),'.jpg'))
        letter=readLetter(Img)
        imshow(Img);
        disp(letter)
        %M = (L==x(i))
        disp('enter')
        pause()
        %imshow(M)
    end
    

end


